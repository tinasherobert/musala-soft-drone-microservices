package com.drones.drone.service.serviceImpl;


import com.drones.drone.domain.exceptions.MedicationErrorException;
import com.drones.drone.domain.models.Medication;
import com.drones.drone.domain.payloads.requests.MedicationRequest;
import com.drones.drone.repository.MedicationRepository;
import com.drones.drone.service.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.drones.drone.domain.constants.Default_Messages.*;

@Service
@RequiredArgsConstructor
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;
    @Override
    public ResponseEntity<String> registerMedication(MedicationRequest medicationPayload) {
        Medication medication = Medication.builder()
                .code(UUID.randomUUID().toString())
                .image(medicationPayload.getImage())
                .weight(medicationPayload.getWeight())
                .name(medicationPayload.getName())
                .build();
        medicationRepository.save(medication);
        return ResponseEntity.ok().body(MEDICATION_CREATED_SUCCESS);
    }

    @Override
    public ResponseEntity<Medication> getMedication(Long id) {
        Medication medication = medicationRepository.findById(id).orElseThrow(()-> new MedicationErrorException(MEDICATION_NOT_FOUND));
        return ResponseEntity.ok().body(medication);
    }

    @Override
    public ResponseEntity <List<Medication>> getAllMedications() {
        List<Medication> medList = medicationRepository.findAll();
        if (medList.isEmpty()) throw new MedicationErrorException(MEDICATION_NOT_AVAILABLE);
        return ResponseEntity.ok().body(medList);
    }

    @Override
    public Optional<Medication> getMedicationById(Long medId) {
        return medicationRepository.findById(medId);
    }

}
