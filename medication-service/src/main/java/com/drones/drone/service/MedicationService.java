package com.drones.drone.service;



import com.drones.drone.domain.models.Medication;
import com.drones.drone.domain.payloads.requests.MedicationRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface MedicationService {
    ResponseEntity<String> registerMedication(MedicationRequest medicationPayload);
    ResponseEntity<Medication> getMedication(Long id);
    ResponseEntity<List<Medication>> getAllMedications();

    Optional<Medication> getMedicationById(Long medId);
}
