package com.drones.drone.config.data;

import com.drones.drone.domain.enums.DroneModels;
import com.drones.drone.domain.enums.DroneStates;
import com.drones.drone.domain.models.Drone;
import com.drones.drone.domain.models.Medication;
import com.drones.drone.repository.MedicationRepository;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Configuration
public class InitialDataMedicationConfiguration {

    private final MedicationRepository medicationRepository;

    public InitialDataMedicationConfiguration(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Started after Spring boot application !");

        if(medicationRepository.count()< 1 ) {
            medicationRepository.saveAll(medicationDb());
        }
    }


    public static List<Drone> droneDb() {
        List<Drone> drones = new ArrayList<>();
        drones.add(Drone.builder()
                .droneModel(DroneModels.LIGHT_WEIGHT)
                .droneState(DroneStates.IDLE)
                .serialNumber(UUID.randomUUID().toString().toUpperCase())
                .droneWeigh(0.0)
                .batteryPercentage(100)
                .build());
        drones.add(Drone.builder()
                .droneModel( DroneModels.MIDDLE_WEIGHT)
                .droneState(DroneStates.IDLE)
                .serialNumber(UUID.randomUUID().toString().toUpperCase())
                .droneWeigh(0.0)
                .batteryPercentage(65)
                .build());
        drones.add(Drone.builder()
                .droneModel( DroneModels.CRUISER_WEIGHT)
                .droneState(DroneStates.IDLE)
                .serialNumber(UUID.randomUUID().toString().toUpperCase())
                .droneWeigh(0.0)
                .batteryPercentage(30)
                .build());
        drones.add(Drone.builder()
                .droneModel( DroneModels.HEAVY_WEIGHT)
                .droneState(DroneStates.IDLE)
                .serialNumber(UUID.randomUUID().toString().toUpperCase())
                .droneWeigh(0.0)
                .batteryPercentage(50)
                .build());
        return drones;
    }


    public static List<Medication> medicationDb() {
        ArrayList<Medication> medList = new ArrayList<>();

        medList.add(Medication.builder()
                .name( "Paracetamol")
                .weight(20.0)
                .code("PARACET_123")
                .image("paracetamol.png")
                .build());
        medList.add(Medication.builder()
                .name( "Sanitizer")
                .weight(24.0)
                .code("SANITIZER")
                .image("Sanitizer.png")
                .build());
        medList.add(Medication.builder()
                .name( "Iodine")
                .weight(10.0)
                .code("IODINE")
                .image("Iodine.png")
                .build());
        medList.add(Medication.builder()
                .name( "Dettol")
                .weight(27.0)
                .code("DETTOL")
                .image("Dettol.png")
                .build());
        medList.add(Medication.builder()
                .name( "Tetraciclin")
                .weight(15.0)
                .code("TETRACI")
                .image("Tetraciclin.png")
                .build());
        medList.add(Medication.builder()
                .name( "Amoxilin")
                .weight(80.0)
                .code("AMOXILIN")
                .image("Amoxilin.png")
                .build());
        medList.add(Medication.builder()
                .name( "Panadol")
                .weight(12.0)
                .code("PANADOL")
                .image("Panadol.png")
                .build());
        return medList;
    }


}