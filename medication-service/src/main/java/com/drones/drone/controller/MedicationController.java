package com.drones.drone.controller;


import com.drones.drone.domain.models.Medication;
import com.drones.drone.domain.payloads.requests.MedicationRequest;
import com.drones.drone.service.MedicationService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class MedicationController {
private final MedicationService medicationService;

    @Operation(summary = "This registers a new Medication and add to db")
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> registerMedication(@RequestBody MedicationRequest medicationPayload){
        return medicationService.registerMedication(medicationPayload);
    }

    @Operation(summary = "This will get a Medication with an Id")
    @GetMapping("get-medication")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Medication> getMedication(@RequestParam("med_id")Long medId){
        return medicationService.getMedication(medId);
    }

    @Operation(summary = "This will get a Medication with an Id")
    @GetMapping("findById")
    @ResponseStatus(HttpStatus.OK)
    public Optional<Medication> getMedicationById(@RequestParam("med_id")Long medId){
        return medicationService.getMedicationById(medId);
    }

    @Operation(summary = "This will get all medications")
    @GetMapping("get-all-medication")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Medication>> getAllMedications(){
        return medicationService.getAllMedications();
    }
}
