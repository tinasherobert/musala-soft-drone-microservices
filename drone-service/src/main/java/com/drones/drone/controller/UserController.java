package com.drones.drone.controller;

import com.drones.drone.domain.payloads.requests.UserRequest;
import com.drones.drone.domain.payloads.responses.UserResponse;
import com.drones.drone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

	@Autowired
	private UserService userService;


	@RequestMapping(path = "/", method = RequestMethod.POST)
	public UserResponse createNewAccount(@Valid @RequestBody UserRequest user) {
		return userService.create(user);
	}
}
