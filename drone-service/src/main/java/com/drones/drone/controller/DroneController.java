package com.drones.drone.controller;


import com.drones.drone.domain.models.Drone;
import com.drones.drone.domain.models.Medication;
import com.drones.drone.domain.payloads.requests.DroneRequest;
import com.drones.drone.domain.payloads.requests.MedicationRequest;
import com.drones.drone.domain.payloads.responses.DroneResponse;
import com.drones.drone.service.DroneService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class DroneController {
private final DroneService droneService;

    @Operation(summary = "This registers a new drone and add to db")
    @PostMapping("/register-drone")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<DroneResponse> registerDrone(@RequestBody DroneRequest droneDto) {
        return droneService.registerDrone(droneDto);
    }

    @Operation(summary = "This gets a drone with a particular Id")
    @GetMapping("get-drone")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Drone> getDrones(@RequestParam("drone-id")Long id){

        return droneService.getDrone(id);
    }

    @Operation(summary = "This fetches all the drones from the DB")
    @GetMapping("get-all-drones")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Drone>> getAllDrones() {
        return droneService.getAllDrones();
    }

    @Operation(summary = "This loads a medication to a drone with medicationId and droneId")
    @PostMapping("load-drone")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> loadDroneWithMedicationItems(@RequestParam("drone-id")Long droneId, @RequestParam("med-id")Long medicationId) {
        return droneService.loadDroneWithMedicationItems(droneId,medicationId);
    }

    @Operation(summary = "This will fetch all the Medication assigned to a drone")
    @GetMapping("get-medication-on-drone")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Medication>> getMedicationItemsOnDrone(@RequestParam("drone-id")Long droneId) {
        return droneService.getMedicationItemsOnDrone(droneId);
    }

    @Operation(summary = "This will get all the drones available for load ")
    @GetMapping("available-drones-for-loading")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Drone>> getAvailableDronesForLoading() {
        return droneService.getAvailableDronesForLoading();
    }

    @Operation(summary = "This will display the battery level of a drone")
    @GetMapping("drone-battery-level")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> getDroneBatteryLevel(@RequestParam("drone-id")Long droneId) {
        return droneService.getDroneBatteryLevel(droneId);
    }


    @Operation(summary = "This registers a new Medication and add to db")
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> registerMedication(@RequestBody MedicationRequest medicationPayload){
        return droneService.registerMedication(medicationPayload);
    }

    @Operation(summary = "This will get a Medication with an Id")
    @GetMapping("get-medication")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Medication> getMedication(@RequestParam("med_id")Long medId){
        return droneService.getMedication(medId);
    }

    @Operation(summary = "This will get all medications")
    @GetMapping("get-all-medication")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Medication>> getAllMedications(){
        return droneService.getAllMedications();
    }
}
