package com.drones.drone.service;


import com.drones.drone.domain.models.Drone;
import com.drones.drone.domain.models.Medication;
import com.drones.drone.domain.payloads.requests.DroneRequest;
import com.drones.drone.domain.payloads.requests.MedicationRequest;
import com.drones.drone.domain.payloads.responses.DroneResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface DroneService {
    ResponseEntity<DroneResponse> registerDrone(DroneRequest droneRequest);
    ResponseEntity<Drone> getDrone(Long id);
    ResponseEntity<List<Drone>> getAllDrones();
    ResponseEntity<String> loadDroneWithMedicationItems(Long droneId, Long medicationId);
    ResponseEntity<List<Medication>> getMedicationItemsOnDrone(Long droneId);
    ResponseEntity<List<Drone>> getAvailableDronesForLoading();
    ResponseEntity<String> getDroneBatteryLevel(Long droneId);

    ResponseEntity<String> registerMedication(MedicationRequest medicationPayload);

    ResponseEntity<Medication> getMedication(Long medId);

    ResponseEntity<List<Medication>> getAllMedications();
}
