package com.drones.drone.service.serviceImpl;



import com.drones.drone.client.MedicationServiceClient;
import com.drones.drone.domain.models.Drone;
import com.drones.drone.domain.models.Medication;
import com.drones.drone.domain.enums.DroneStates;
import com.drones.drone.domain.exceptions.DroneErrorException;
import com.drones.drone.domain.exceptions.MedicationErrorException;
import com.drones.drone.domain.payloads.requests.DroneRequest;
import com.drones.drone.domain.payloads.requests.MedicationRequest;
import com.drones.drone.domain.payloads.responses.DroneResponse;
import com.drones.drone.repository.DroneRepository;
import com.drones.drone.service.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.drones.drone.domain.constants.Default_Messages.*;

@Service
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {
    private final DroneRepository droneRepository;
    private final MedicationServiceClient medicationServiceClient;

    @Override
    public ResponseEntity<DroneResponse> registerDrone(DroneRequest droneRequest) {

        Drone drone = Drone.builder()
                .droneModel(droneRequest.getModel())
                .droneState(DroneStates.IDLE)
                .serialNumber(UUID.randomUUID().toString().toUpperCase())
                .droneWeigh(0.0)
                .batteryPercentage(100)
                .build();
        droneRepository.save(drone);
        DroneResponse droneResponse = mapToDroneRequest(drone);
        return new ResponseEntity<>(droneResponse, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Drone> getDrone(Long id) {
        Drone drone = droneRepository.findById(id).orElseThrow(() -> new DroneErrorException(DRONE_NOT_FOUND));
        return ResponseEntity.ok().body(drone);
    }

    @Override
    public ResponseEntity<List<Drone>> getAllDrones() {
        List<Drone> drone = droneRepository.findAll();
        if(drone.isEmpty()) throw new DroneErrorException(DRONE_NOT_AVAILABLE);
        return ResponseEntity.ok().body(drone);
    }

    @Override
    @Transactional
    public ResponseEntity<String> loadDroneWithMedicationItems(Long droneId, Long medicationId) {
        Drone drone = droneRepository.findById(droneId).orElseThrow(() -> new DroneErrorException(DRONE_NOT_FOUND));
        Medication medication = medicationServiceClient.findMedicationById(medicationId).orElseThrow(() -> new MedicationErrorException(MEDICATION_NOT_FOUND));

        if (medication.getDrone() != null) throw new MedicationErrorException(MEDICATION_ALREADY_LOADED);
        validateDroneAvailability(drone,medication);

        droneRepository.save(drone);
        return ResponseEntity.ok().body(MEDICATION_LOADED_SUCCESSFUL);
    }

    @Override
    public ResponseEntity<List<Medication>> getMedicationItemsOnDrone(Long droneId) {

        Drone drone  = droneRepository.findById(droneId).orElseThrow(()-> new DroneErrorException(DRONE_NOT_FOUND));
        List<Medication> medication = drone.getMedications();
        if(medication.isEmpty()) throw new MedicationErrorException(MEDICATION_NOT_AVAILABLE);
        return ResponseEntity.ok().body(medication);
    }

    @Override
    public ResponseEntity<List<Drone>> getAvailableDronesForLoading() {

        List<Drone> drones = droneRepository.findAll();
        if(drones.isEmpty()) throw new DroneErrorException(DRONE_NOT_AVAILABLE);
        List<Drone> availableDrones = drones.stream().filter( d -> d.getDroneState().equals(DroneStates.IDLE)
                        || d.getDroneState().equals(DroneStates.LOADING))
                .collect( Collectors.toList());

        return ResponseEntity.ok().body(availableDrones);
    }

    @Override
    public ResponseEntity<String> getDroneBatteryLevel(Long droneId) {
        Drone drone  = droneRepository.findById(droneId).orElseThrow(()-> new DroneErrorException(DRONE_NOT_FOUND));
        int batteryLevel = drone.getBatteryPercentage();
        return ResponseEntity.ok().body(String.format(DRONE_BATTERY_LEVEL,batteryLevel));
    }

    @Override
    public ResponseEntity<String> registerMedication(MedicationRequest medicationPayload) {
        return medicationServiceClient.registerMedication(medicationPayload);
    }

    @Override
    public ResponseEntity<Medication> getMedication(Long medId) {
        return medicationServiceClient.getMedication(medId);
    }

    @Override
    public ResponseEntity<List<Medication>> getAllMedications() {
        return medicationServiceClient.getAllMedications();
    }

    private DroneResponse mapToDroneRequest(Drone drone) {

        return DroneResponse.builder()
                .serialNumber(drone.getSerialNumber())
                .droneState(drone.getDroneState())
                .droneModel(drone.getDroneModel())
                .droneWeigh(drone.getDroneWeigh())
                .batteryPercentage(drone.getBatteryPercentage())
                .build();
    }

    private void validateDroneAvailability(Drone drone, Medication medication) {
        double capacity = drone.getDroneModel().getWeight();
        double droneWeigh = drone.getDroneWeigh();
        double loadAccumulator = droneWeigh + medication.getWeight();

        if (drone.getBatteryPercentage() < 25) {
            throw new DroneErrorException(DRONE_BATTERY_LOW);
        }
        if (loadAccumulator > capacity) {
            throw new DroneErrorException(CAPACITY_EXCEEDED);
        }
        if (!(drone.getDroneState().equals(DroneStates.IDLE) || drone.getDroneState().equals(DroneStates.LOADING))) {
            throw new DroneErrorException(DRONE_NOT_AVAILABLE);
        }

        drone.getMedications().add(medication);
        loadAccumulator +=  medication.getWeight();
        drone.setDroneWeigh(loadAccumulator);
        medication.setDrone(drone);

        if(loadAccumulator < capacity){
            drone.setDroneState(DroneStates.LOADING);
        } else {
            drone.setDroneState(DroneStates.LOADED);
        }
    }
}
