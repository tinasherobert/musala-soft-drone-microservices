package com.drones.drone.service;

import com.drones.drone.domain.payloads.requests.UserRequest;
import com.drones.drone.domain.payloads.responses.UserResponse;

public interface UserService {


	/**
	 * Checks if account with the same name already exists
	 * Invokes Auth Service user creation
	 * Creates new account with default parameters
	 *
	 * @param user
	 * @return created account
	 */
	UserResponse create(UserRequest user);

}
