package com.drones.drone.service.serviceImpl;

import com.drones.drone.domain.payloads.requests.UserRequest;
import com.drones.drone.domain.payloads.responses.UserResponse;
import com.drones.drone.client.AuthServiceClient;
import com.drones.drone.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

	private final Logger log = LoggerFactory.getLogger(getClass());


	@Autowired
	private AuthServiceClient authClient;



	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserResponse create(UserRequest user) {
		authClient.createUser(user);
		log.info("new account has been created: " + user.getUsername());
		UserResponse userResponse = new UserResponse() ;
		userResponse.setUsername(user.getUsername());
		userResponse.setPassword("XXXXXXXXXXXXXXXXXX");
		return userResponse;
	}

}
