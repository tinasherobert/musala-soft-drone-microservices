package com.drones.drone.config.data;

import com.drones.drone.domain.enums.DroneModels;
import com.drones.drone.domain.enums.DroneStates;
import com.drones.drone.domain.models.Drone;
import com.drones.drone.repository.DroneRepository;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Configuration
public class InitialDataConfiguration {

    private final DroneRepository droneRepository;

    public InitialDataConfiguration(DroneRepository droneRepository) {
        this.droneRepository = droneRepository;
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Started after Spring boot application !");

        if(droneRepository.count()< 1 ) {
            droneRepository.saveAll(droneDb());
        }
    }


    public static List<Drone> droneDb() {
        List<Drone> drones = new ArrayList<>();
        drones.add(Drone.builder()
                .droneModel(DroneModels.LIGHT_WEIGHT)
                .droneState(DroneStates.IDLE)
                .serialNumber(UUID.randomUUID().toString().toUpperCase())
                .droneWeigh(0.0)
                .batteryPercentage(100)
                .build());
        drones.add(Drone.builder()
                .droneModel( DroneModels.MIDDLE_WEIGHT)
                .droneState(DroneStates.IDLE)
                .serialNumber(UUID.randomUUID().toString().toUpperCase())
                .droneWeigh(0.0)
                .batteryPercentage(65)
                .build());
        drones.add(Drone.builder()
                .droneModel( DroneModels.CRUISER_WEIGHT)
                .droneState(DroneStates.IDLE)
                .serialNumber(UUID.randomUUID().toString().toUpperCase())
                .droneWeigh(0.0)
                .batteryPercentage(30)
                .build());
        drones.add(Drone.builder()
                .droneModel( DroneModels.HEAVY_WEIGHT)
                .droneState(DroneStates.IDLE)
                .serialNumber(UUID.randomUUID().toString().toUpperCase())
                .droneWeigh(0.0)
                .batteryPercentage(50)
                .build());
        return drones;
    }



}