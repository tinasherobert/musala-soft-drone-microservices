package com.drones.drone.client;

import com.drones.drone.domain.models.Medication;
import com.drones.drone.domain.payloads.requests.MedicationRequest;
import com.drones.drone.domain.payloads.requests.UserRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Optional;

@FeignClient(name = "medication-service", fallback = MedicationServiceClientFallback.class)
public interface MedicationServiceClient {

	@RequestMapping(method = RequestMethod.GET, value = "/api/v1/findById/{medicationId}")
    Optional<Medication> findMedicationById(@PathVariable("medicationId") Long medicationId);

    @RequestMapping(method = RequestMethod.GET, value = "/api/v1/get-medication/{med_id}")
    ResponseEntity<Medication> getMedication(@PathVariable("med_id") Long medicationId);

	@RequestMapping(method = RequestMethod.GET, value = "/api/v1/get-all-medication")
    ResponseEntity<List<Medication>> getAllMedications();

    @RequestMapping(method = RequestMethod.POST, value = "/api/v1/register", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> registerMedication(@RequestBody MedicationRequest medicationPayload);

}
