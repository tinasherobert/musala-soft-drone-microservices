package com.drones.drone.client;


import com.drones.drone.domain.models.Medication;
import com.drones.drone.domain.payloads.requests.MedicationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author tinashe makaza
 */
@Component
public class MedicationServiceClientFallback implements MedicationServiceClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationServiceClientFallback.class);
    @Override
    public Optional<Medication> findMedicationById(Long medicationId) {
        LOGGER.error("Error during getting medication id: {}", medicationId);
        return Optional.empty();
    }

    @Override
    public ResponseEntity<Medication> getMedication(Long medicationId) {
        LOGGER.error("Error during getting medication id: {}", medicationId);
        return null;
    }

    @Override
    public ResponseEntity<List<Medication>> getAllMedications() {
        LOGGER.error("Error during getting all medication");
        return null;
    }

    @Override
    public ResponseEntity<String> registerMedication(MedicationRequest medicationPayload) {
        LOGGER.error("Error during registering medication id: {}", medicationPayload);
        return null;
    }
}
