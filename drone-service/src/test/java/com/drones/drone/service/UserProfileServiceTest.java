package com.drones.drone.service;

import com.drones.drone.domain.payloads.requests.UserRequest;
import com.drones.drone.domain.payloads.responses.UserResponse;
import com.drones.drone.client.AuthServiceClient;
import com.drones.drone.service.serviceImpl.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class UserServiceTest {

	@InjectMocks
	private UserServiceImpl accountService;

	@Mock
	private AuthServiceClient authClient;


	@Before
	public void setup() {
		initMocks(this);
	}




	@Test
	public void shouldCreateAccountWithGivenUser() {

		UserRequest user = new UserRequest();
		user.setUsername("test");

		UserResponse account = accountService.create(user);

		verify(authClient, times(1)).createUser(user);
	}

}
