package com.drones.drone.service;

import com.drones.drone.domain.models.UserProfile;
import com.drones.drone.repository.UserProfileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserProfileServiceImpl implements UserProfileService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	@Autowired
	private UserProfileRepository repository;

	@Override
	public void create(UserProfile userProfile) {

		Optional<UserProfile> existing = repository.findByUsername(userProfile.getUsername());
		existing.ifPresent(it-> {throw new IllegalArgumentException("user already exists: " + it.getUsername());});

		String hash = encoder.encode(userProfile.getPassword());
		userProfile.setPassword(hash);

		repository.save(userProfile);

		log.info("new user has been created: {}", userProfile.getUsername());
	}
}
