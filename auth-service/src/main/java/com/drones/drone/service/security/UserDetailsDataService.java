package com.drones.drone.service.security;

import com.drones.drone.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsDataService implements org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	private UserProfileRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		return repository.findByUsername(username).orElseThrow(()->new UsernameNotFoundException(username));
	}
}
