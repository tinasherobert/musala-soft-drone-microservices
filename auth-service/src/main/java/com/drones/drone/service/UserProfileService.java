package com.drones.drone.service;

import com.drones.drone.domain.models.UserProfile;

public interface UserProfileService {

	void create(UserProfile userProfile);

}
