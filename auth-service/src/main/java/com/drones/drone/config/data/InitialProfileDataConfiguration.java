package com.drones.drone.config.data;

import com.drones.drone.domain.models.UserProfile;
import com.drones.drone.repository.UserProfileRepository;
import com.drones.drone.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class InitialProfileDataConfiguration {

    private final UserProfileService userProfileService;
    private final UserProfileRepository userProfileRepository ;

    public InitialProfileDataConfiguration(UserProfileService userProfileService, UserProfileRepository userProfileRepository) {
        this.userProfileService = userProfileService;
        this.userProfileRepository = userProfileRepository;
    }


    @PostConstruct
    public void postConstruct() {
        System.out.println("Started after Spring boot application !");
        if(userProfileRepository.count() < 1) {
            profiles().forEach(userProfileService::create);
        }
    }


    public static List<UserProfile> profiles() {
        List<UserProfile> profiles = new ArrayList<>();
        profiles.add(UserProfile.builder()
                .username("tinashe")
                .password("tinashe")
                .build());
        profiles.add(UserProfile.builder()
                .username("admin")
                .password("admin")
                .build());
        profiles.add(UserProfile.builder()
                .username("demo")
                .password("demo")
                .build());
        profiles.add(UserProfile.builder()
                .username("user")
                .password("password")
                .build());
        return profiles;
    }



}