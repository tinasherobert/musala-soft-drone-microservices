package com.drones.auth.service;

import com.drones.drone.domain.models.UserProfile;
import com.drones.drone.service.UserServiceImpl;
import com.drones.drone.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class UserProfileProfileServiceTest {

	@InjectMocks
	private UserServiceImpl userService;

	@Mock
	private UserRepository repository;

	@Before
	public void setup() {
		initMocks(this);
	}

	@Test
	public void shouldCreateUser() {

		UserProfile userProfile = new UserProfile();
		userProfile.setUsername("name");
		userProfile.setPassword("password");

		userService.create(userProfile);
		verify(repository, times(1)).save(userProfile);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailWhenUserAlreadyExists() {

		UserProfile userProfile = new UserProfile();
		userProfile.setUsername("name");
		userProfile.setPassword("password");

		when(repository.findByUsername(userProfile.getUsername())).thenReturn(Optional.of(new UserProfile()));
		userService.create(userProfile);
	}
}
