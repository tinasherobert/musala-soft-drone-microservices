package com.drones.auth.repository;

import com.drones.drone.domain.models.UserProfile;
import com.drones.drone.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataMongoTest
public class UserProfileProfileRepositoryTest {

	@Autowired
	private UserProfileRepository repository;

	@Test
	public void shouldSaveAndFindUserByName() {

		UserProfile userProfile = new UserProfile();
		userProfile.setUsername("name");
		userProfile.setPassword("password");
		repository.save(userProfile);

		Optional<UserProfile> found = repository.findByUsername(userProfile.getUsername());
		assertTrue(found.isPresent());
		assertEquals(userProfile.getUsername(), found.get().getUsername());
		assertEquals(userProfile.getPassword(), found.get().getPassword());
	}
}
