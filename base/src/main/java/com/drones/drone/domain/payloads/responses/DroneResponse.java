package com.drones.drone.domain.payloads.responses;


import com.drones.drone.domain.enums.DroneModels;
import com.drones.drone.domain.enums.DroneStates;
import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DroneResponse {

    private DroneModels droneModel;
    private DroneStates droneState;
    private String serialNumber;
    private Double droneWeigh;
    private Integer batteryPercentage;
}
