package com.drones.drone.domain.payloads.requests;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MedicationRequest {
    private String name;
    private Double weight;
    private String image;
}
