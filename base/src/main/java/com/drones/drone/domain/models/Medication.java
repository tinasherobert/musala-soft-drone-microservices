package com.drones.drone.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class Medication extends AbstractEntity {

    @Pattern(regexp="[a-zA-Z0-9_-]+",message="allows only letters, numbers, ‘-‘ and ‘_")
    private String name;
    private Double weight;
    @Pattern(regexp="[A-Z0-9_]+",message="allows only upper case, numbers and ‘_")
    private String code;
    private String image;
    @ManyToOne
    @JoinColumn(name = "drone_id")
    @JsonIgnore
    private Drone drone;
}
