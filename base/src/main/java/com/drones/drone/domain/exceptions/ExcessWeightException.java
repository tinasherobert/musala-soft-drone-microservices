package com.drones.drone.domain.exceptions;

public class ExcessWeightException extends RuntimeException{

    public ExcessWeightException(String message){
        super(message);
    }
}
