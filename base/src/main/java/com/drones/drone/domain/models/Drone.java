package com.drones.drone.domain.models;

import com.drones.drone.domain.enums.DroneModels;
import com.drones.drone.domain.enums.DroneStates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class Drone  extends AbstractEntity{

    @Size(min = 1, max = 100)
    @NotNull(message = "please enter a serial number for the drone")
    private String serialNumber;
    @Enumerated(EnumType.STRING)
    private DroneModels droneModel;
    @Range(min=0, max=500)
    private Double droneWeigh;
    private Integer batteryPercentage;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "varchar(20) default 'IDLE'")
    private DroneStates droneState;
    @OneToMany(mappedBy = "drone", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnore
    List<Medication> medications;

}