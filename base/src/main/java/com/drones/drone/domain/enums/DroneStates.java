package com.drones.drone.domain.enums;

public enum DroneStates {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
