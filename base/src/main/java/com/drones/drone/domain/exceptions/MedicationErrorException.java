package com.drones.drone.domain.exceptions;

public class MedicationErrorException extends RuntimeException{
    public MedicationErrorException(String message){
        super(message);
    }
}
