package com.drones.drone.domain.payloads.responses;

import lombok.Data;

@Data
public class UserResponse {

    String username ;
    String password ;
}
