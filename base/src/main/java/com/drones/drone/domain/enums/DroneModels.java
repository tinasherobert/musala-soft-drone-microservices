package com.drones.drone.domain.enums;

import lombok.Getter;

@Getter
public enum DroneModels {

    LIGHT_WEIGHT (100.0),
    MIDDLE_WEIGHT (200.0),
    CRUISER_WEIGHT (350.0),
    HEAVY_WEIGHT (500.0);

    private final double weight;

    DroneModels (double weight){
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }
}
