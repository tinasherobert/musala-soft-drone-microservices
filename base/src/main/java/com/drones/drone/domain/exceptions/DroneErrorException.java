package com.drones.drone.domain.exceptions;

public class DroneErrorException extends RuntimeException{
    public DroneErrorException(String message){
        super(message);
    }
}