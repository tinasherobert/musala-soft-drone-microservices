package com.drones.drone.domain.models;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(
            name = "sequenceGenerator",
            strategy = "enhanced-sequence",
            parameters = {
                    @org.hibernate.annotations.Parameter(
                            name = "optimizer",
                            value = "pooled-lo"
                    ),
                    @org.hibernate.annotations.Parameter(
                            name = "initial_value",
                            value = "5000000"
                    ),
                    @org.hibernate.annotations.Parameter(
                            name = "increment_size",
                            value = "1000"
                    )
            }
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequenceGenerator"
    )
	protected long id;
    @Column(length = 50)
    private String status;
 
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date createdAt;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date updatedAt;
    //private boolean deleted = false;
    @Version
    private int version;

    @PrePersist
    protected void onCreate() {
        this.createdAt = new Date(System.currentTimeMillis());
        this.updatedAt = this.createdAt;
    }

    @PreUpdate
    protected void onUpdate() {
        this.updatedAt = new Date(System.currentTimeMillis());
    }

}

