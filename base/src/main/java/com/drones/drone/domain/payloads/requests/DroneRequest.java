package com.drones.drone.domain.payloads.requests;


import com.drones.drone.domain.enums.DroneModels;
import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DroneRequest {

    private DroneModels model;
}
