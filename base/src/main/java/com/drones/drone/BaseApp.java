package com.drones.drone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */

@SpringBootApplication
public class BaseApp
{
    public static void main(String[] args) {
        SpringApplication.run(BaseApp.class, args);
    }
}

